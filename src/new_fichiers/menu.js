// SCRIPT TO INSERT IN DEVELOPER CONSOLE FOR TEST
// var menuJS = document.createElement('script');
// menuJS.type = 'text/javascript';
// menuJS.async = true;
// menuJS.src = 'https://ent.uca.fr/core/menu.js';
// document.head.appendChild(menuJS);

var touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
var touchEvents = ['touchstart','click'];
var entHost = 'https://ent.uca.fr';

var xhrMenu = new XMLHttpRequest();
xhrMenu.onreadystatechange = function() {
    if (xhrMenu.readyState == XMLHttpRequest.DONE ) {
        if (xhrMenu.status == 200) {
            // no session no menu
            var menuHtml = xhrMenu.responseText;
            if (menuHtml) {
                // insert rendered menu
                var ent = document.createElement('div');
                ent.setAttribute('id', 'ent_menu');
                ent.innerHTML = xhrMenu.responseText;
                // if return CAS, remove all content
                var contentCAS = ent.querySelector('#content.cas-center');
                if (contentCAS) {
                    // enable on ent-test to get CAS server
                    var href = window.location.hostname == 'ent-test.uca.fr' ? 'https://ent-test.uca.fr' : 'https://ent.uca.fr';
                    href = href + '/cas/login?service=' + window.location.href;
                    window.location.href = href;
                    // document.body.innerHTML = '';
                    return false;
                }
                document.body.insertBefore(ent, document.body.firstChild);

                var entMenuToast = document.getElementById('ent_menu_toast');

                // Bug if only css so use JS to slider effect ...
                var menu    = document.getElementById('ent_menu_top_open');
                var sidebar = document.getElementById('ent_menu_sidebar');
                var close   = document.getElementById('ent_menu_sidebar_close');
                if (menu && sidebar && close) {
                    for (var i = 0; i < touchEvents.length; i++) {
                        window.addEventListener(touchEvents[i], function(event) {
                            if (close.contains(event.target)) { // || !sidebar.contains(event.target)) {
                                sidebar.setAttribute('class', 'nothover');
                            }
                            if (menu.contains(event.target)) {
                                sidebar.setAttribute('class', 'hover');
                            }
                        });
                    }
                }

                // Access count
                var accessMenus = document.querySelectorAll('[data-access]');
                for (var i = 0; i < accessMenus.length; i++) {
                    var accessMenu = accessMenus[i];
                    for (var j = 0; j < touchEvents.length; j++) {
                        accessMenu.addEventListener(touchEvents[j], function(event) {
                            var accessMenuClicked = this;
                            var xhrMenuAccessCount = new XMLHttpRequest();
                            xhrMenuAccessCount.onreadystatechange = function() {
                                if (xhrMenuAccessCount.readyState == XMLHttpRequest.DONE ) {
                                    console.log('Access count menu', accessMenuClicked.dataset.access);
                                }
                            };
                            xhrMenuAccessCount.open('POST', entHost + '/core/menu/' + accessMenuClicked.dataset.access + '/count', true);
                            xhrMenuAccessCount.send();
                        });
                    }
                    
                }

                // Logout
                var logoutUrls = [entHost + '/compte/logout'];// by default, it's not an item, it's in menu
                var logoutApps = document.querySelectorAll('[data-logout]');
                for (var i = 0; i < logoutApps.length; i++) {
                    var logoutUrlsApp = logoutApps[i].dataset.logout.split('\n');
                    for (var j = 0; j < logoutUrlsApp.length; j++) {
                        logoutUrls.push(logoutUrlsApp[j]);
                    }
                }
                var logoutButtons = document.querySelectorAll('.logoutButton');
                for (var i = 0; i < logoutButtons.length; i++) {
                    var logoutButton = logoutButtons[i];
                    for (var j = 0; j < touchEvents.length; j++) {
                        logoutButton.addEventListener(touchEvents[j], function(event) {
                            // Loading
                            console.log('Loading effect');
                            var loading = document.createElement('div');
                            loading.setAttribute('id', 'loading_page');
                            loading.innerHTML = '<span>D&eacute;connexion des services ...</span>';
                            document.body.insertBefore(loading, document.body.firstChild);
                            // Call logout for each application
                            for (var k = 0; k < logoutUrls.length; k++) {
                                var logoutUrl = logoutUrls[k];
                                var xhrLogout = new XMLHttpRequest();
                                xhrLogout.onreadystatechange = function() {
                                    if (xhrLogout.readyState == XMLHttpRequest.DONE ) {
                                        console.log('Deconnect', logoutUrl);
                                    }
                                };
                                xhrLogout.open('GET', logoutUrl, true);
                                xhrLogout.send();
                            }
                            // Redirect to logout ent
                            setTimeout(function() {
                                console.log('Redirect to logout ENT');
                                window.open(entHost + '/core/logout', '_self');
                            }, 1000);
                        });
                    }
                }

                //Favorite
                var entFavorite = document.getElementById('ent_favorite');
                if (entFavorite) {
                    var xhrFavorite = new XMLHttpRequest();
                    xhrFavorite.onreadystatechange = function() {
                        if (xhrFavorite.readyState == XMLHttpRequest.DONE ) {
                            if (xhrFavorite.status == 200) {
                                // change favorite icon
                                entFavorite.firstChild.innerHTML = xhrFavorite.responseText == '1' ? 'favorite' : 'favorite_border';
                                entFavorite.setAttribute('data-action', xhrFavorite.responseText == '1' ? 'remove' : 'add');
                                entFavorite.setAttribute('title', xhrFavorite.responseText == '1' ? 'Supprimer de mes favoris' : 'Mettre en favoris');
                                entMenuToast.innerHTML = xhrFavorite.responseText == '1' ? 'Ajout&eacute; dans mes favoris<br>Apr&egrave;s rafraichissement il appara&icirc;tra dans votre menu' : 'Supprim&eacute; de mes favoris<br>Apr&egrave;s rafraichissement il n\'appara&icirc;tra plus dans votre menu';
                                entMenuToast.classList.add('fade');
                            }
                        }
                    };
                    for (var i = 0; i < touchEvents.length; i++) {
                        entFavorite.addEventListener(touchEvents[i], function(event) {
                            // Reset message effect. Time to execute too short if just before add fade
                            entMenuToast.classList.remove('fade');
                            xhrFavorite.open('GET', entHost + '/core/favorite/' + entFavorite.getAttribute('data-action') + '/' + entFavorite.getAttribute('data-id') , true);
                            xhrFavorite.send();
                        });
                    }
                }

            } else {
                console.log('ENT Menu need a connected user');
            }
        } else {
            console.log('ENT Menu not available');
        }
    }
};

var xhrMessage = new XMLHttpRequest();
xhrMessage.onreadystatechange = function() {
    if (xhrMessage.readyState == XMLHttpRequest.DONE ) {
        if (xhrMessage.status == 200) {
            
            // insert rendered message
            var ent = document.createElement('div');
            ent.setAttribute('id', 'ent_message');
            ent.innerHTML = xhrMessage.responseText;
            document.body.insertBefore(ent, document.body.firstChild);

            var closeButton = document.querySelector('#ent_message .popup-close');
            if (closeButton) {
                for (var i = 0; i < touchEvents.length; i++) {
                    closeButton.addEventListener(touchEvents[i], function(event) {
                        // Mark as read and remove message
                        var xhrRead = new XMLHttpRequest();
                        xhrRead.onreadystatechange = function() {
                            if (xhrRead.readyState == XMLHttpRequest.DONE) {
                                if (xhrRead.status == 200) {
                                    document.getElementById('ent_message').remove();
                                }
                            }
                        }
                        xhrRead.open('GET', entHost + '/core/message/' + this.getAttribute('data-message') + '/read/0', true);
                        xhrRead.send();
                    });
                }
            }
            var messageButtons = document.querySelectorAll('#ent_message .popup-action [data-message]');
            if (messageButtons) {
                for (var i = 0; i < messageButtons.length; i++) {
                    var messageButton = messageButtons[i];
                    for (var j = 0; j < touchEvents.length; j++) {
                        messageButton.addEventListener(touchEvents[j], function(event) {
                            var messageButtonClicked = this;
                            // Mark message as read
                            if (messageButtonClicked.getAttribute('data-message')) {
                                var xhrRead = new XMLHttpRequest();
                                xhrRead.onreadystatechange = function() {
                                    if (xhrRead.readyState == XMLHttpRequest.DONE) {
                                        if (xhrRead.status == 200) {
                                            // Send GET request
                                            if (messageButtonClicked.classList.contains('request') && messageButtonClicked.getAttribute('data-url')) {
                                                var xhr = new XMLHttpRequest();
                                                xhr.open('GET', messageButtonClicked.getAttribute('data-url'), true);
                                                xhr.send();
                                            } else if (messageButtonClicked.classList.contains('url')) {
                                                var target = messageButtonClicked.getAttribute('data-url').indexOf(entHost) != -1 ? '_self' : '_blank';
                                                window.open(messageButtonClicked.getAttribute('data-url'), target);
                                            }
                                        }
                                    }
                                }
                                xhrRead.open('GET', entHost + '/core/message/' + messageButtonClicked.getAttribute('data-message') + '/read/' + messageButtonClicked.getAttribute('data-button'), true);
                                xhrRead.send();
                            }
                            // Open url before close - bug on mobile
                            // if (this.getAttribute('href')) {
                            //     window.open(this.getAttribute('href'), this.getAttribute('target'));
                            // }
                            // Remove message
                            document.getElementById('ent_message').remove();
                        });
                    }
                }
            }            
        } else {
            console.log('ENT Message not available');
        }
    }
};


// document.addEventListener("DOMContentLoaded", function(event) {// Don't display menu if loader
window.addEventListener('load', function(event) {

    // LOAD MENU
    // assure encode
    var metaUTF8 = document.createElement('meta');
    metaUTF8.setAttribute('http-equiv', 'Content-Type');
    metaUTF8.setAttribute('content', 'text/html; charset=UTF-8');
    document.head.insertBefore(metaUTF8, document.head.firstChild);
    
    // assure auto resive with devices
    var metaView = document.createElement('meta');
    metaView.setAttribute('name', 'viewport');
    metaView.setAttribute('content', 'width=device-width, initial-scale=1.0'); 
    document.head.insertBefore(metaView, document.head.childNodes[1]);
    
    // insert css before others to prevent failed override
    var linkCSS = document.createElement('link');
    linkCSS.setAttribute('type', 'text/css');
    linkCSS.setAttribute('rel', 'stylesheet');
    linkCSS.setAttribute('href', entHost + '/core/menu.min.css');
    document.head.appendChild(linkCSS);

    xhrMenu.open('GET', entHost + '/core/menu', true);
    xhrMenu.send();

    // HIDE FULL PAGE LOADING
    var loading = document.getElementById('loading_page');
    if (loading)
        loading.classList.add('hidden');

    // DISPLAY GENERATED MESSAGE
    xhrMessage.open('GET', entHost + '/core/message/0/generate', true);
    xhrMessage.send();

});
