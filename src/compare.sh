#!/bin/bash


  
# True scrapper
# ./scrapper.sh

if [ -f "new.html" ]
then
    rm new.html
fi

python3 scrapper.py


v=0

if [ -f "old.html" ]
then
    # diff old.html new.html > /dev/null
    diff old.html new.html

    v=$?
    rm old.html
fi
mv new.html old.html

if [ $v -ne 0 ]
then
    echo "De nouvelles notes sont disponibles !"
fi