#!/bin/bash

curl  'https://ent.uca.fr/cas/login?service=https%3A%2F%2Fent.uca.fr%2Fcore%2Fhome%2F' \
  -H 'authority: ent.uca.fr' \
  -H 'cache-control: max-age=0' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="98"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Linux"' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'origin: https://ent.uca.fr' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-user: ?1' \
  -H 'sec-fetch-dest: document' \
  -H 'referer: https://ent.uca.fr/cas/login?service=https%3A%2F%2Fent.uca.fr%2Fcore%2Fhome%2F' \
  -H 'accept-language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'cookie: JSESSIONID=6F06D594D670A08BCD8201334A4C18E8; PHPSESSID=kup1k0op7nuj0rh1qnhkt6c6q0; device_view=full' \
  --data-raw 'username=simazenoux&password=mSZ52utzuc%21&execution=8327315f-4618-42f5-99ce-0e3c475bceae_ZXlKaGJHY2lPaUpJVXpVeE1pSjkuNllaRldhdU8rNEZsYzZSUk8zekYrTmdCSEoyMWhlZko4eEhwQVhJK21vdUw3bXF4bzY3eW5oeWhaMUUwVWNwNlFMNzREcnFrakVmQy8yT0dIQXVTWXhjam5GQkd1UWFYcVltVXo2OTV0VnVKdDJEdVltU3pNeFJqTTZEOWE4aGhsdWZ2R0QyU09DSUtxUW5oeXNEZmF6R3hmUkhwS0FMNFlDWHlncmY0MW1oUUdscHdodDZMLzQ4ME5rNTZHcWZZcFZ2djZVT2xSRFVwY1hJeHJjcVF0WjlsT3pzc09MT1VMcHN6YkVQTkRmQVlDc3JIcGhwUlZjMzA4L1gzYWpLQjFEWUJuVGdaWS9sd0x5MUVnYVpyV3llN2RPbjAyR3pIUktzUVUyR25pbGtxdTNJeWF0M0NsVXFJajc1VUNmY2llaHlDRHE5d1RneDdNSjdlMExKYzBXblF2S3ZqYjZ1dkx4OFlrd0xBdkhIVk1pU0ZaVFJpZ3Y5RlBFd2w3dHQzMDFuT1ZmSjdva3E3czFUR1BrYlBMaFVwOTEyOXBPemU0Nk5vczJIaG4vekxyRjZpOFRZd0R4VStSaTJvNE1hZmJ5cTcyQnVza2JwNUFkSm1vTmFsbmY5OCtmeU4vTmVlc2VaTVFaZjM5MndhYzl4RkplM1I1OXVQVlRYWWxaQ3ZLU0l0L1NyTnBXa0dqako5QXIwOEd0Z0MyK3UvSnJXUEx1aCtyUXl0U0NOem03SFZaRTV3UXdaMHc5TWgrVEF2MkR0dHZZczlvTS9UTGJQR3F2aTZ6czQxTWpDNndLdVBVZnQzanVHVll4aXpKbjlVNDVmalVldjRQZFNxRC8xSGdtSmZXTlE4d0FwUkVXS2M1Y0FCOXd4YldoN3hTUjVkQWo4N0dVMkxSSkR0Y0xaZ3d0b1ZyVmNBT1BMTlRQUG5NanhRL3IzSWovYkFmcm5tcGtZVTZtL1NiczRYTGRQZ1Y1YXN6SnRYd2JEYU5BYTlXZkZnbzF4K3ppd3hsR09hdVYydGQwTTVwRThQREhidG1TRndyT1JMTVNsN0t0TlFOMFFHWElScnhnWDZrOURDYW9QbFYzSmlWNXhEWDRtUEZ1NXB6bzlVQWtiMDkvYkdEZ1FhSDV6Q0dVT3pLQUR4dktoUm44UHEzVkNoOTZIWjN3UWo4UmV2cnR6NWgwbFRHNFpwVXFPeVdlNWg1YnFhb1Ayb3hVSUVXYUdWc3VkOFNQY1FzbStSWFIxbmdxUjZVNjhVQlQ5QXhwWkR6MmJRM2hGV29iZ3lWb2hFemcvWEppNUhlTWdENm8xUVpzMm9SU25KdXpjcFVLVWh5Y0wvTGhaY3hzVXo5UEw3Z2RxSGJmbGFiYkk3L0tTWUxyM1N6Y3VtZUN3alp2aDE4NjFhYVdmdWtqOHZJckl6MURIT042aHRKdGMzeFBZNExyKzgxRmJhbU5sMkpid1B5dStDcUNRbnlONlNoRFQzRDB4Nk5LeTNVRzBUV1N4ajQxTVd5L3NKSmh0aUNKbEpCQW90Z1F3dnE1eVV3dGNNN1R1b3AwdzZHOUppa0M3OXhMUWFMdE16eWlnSGhTd3pnOFdzcWlON2doeDQvTFJybHQ5WjkvOGxSTjBqdThJYU51eXFtRnRPbmZhR2NNYXdvclZGN21lMFdueGVLVHl3ZGdUanNQZGU0VGU4Skl2VlVPU0tZQks0a00xTWtnTGU2Q3FvUUlpQ3FSN1Y1WTBSTThHNFo0ZGFkdUtvbFNlRThxVXBtbHc5dUxYZWxYalJ0cjhaSVNMT1E4T3ozVGowUTkvbTM1NUU5ek1Pa01XL1FlWXFGcjdHS01QWnJNNWh1cVNFTkR3cHV4U25KK0xwYWNRMjRGTWdLV0VGRjF3cXV1c1NnUnNuK25tc2Q5TXpXNWhaTTBvT1R1eEg2S0JpU2VpRnlwazdzTHlZaUZsbzF0b0VianRwejNBcmdoSlBQMnFZeXBDM2x4Nk9nSDlqdCs5bWNUbFVsTXg1VXZjYWkzUThFMkV4SzJ1OTF2MHd0L2IzQmh5SEtjYzdZd09QSnU5Q3BZODE5WXhyR1BGc0VBRlJieTRPOTl1b2NwRzN2b2dUcXZDSzNuYmxtRUVzYjNXWlB0T1EreUR3ZkpIRUZhOWtTbitNVmQ4ajNiV0w5clBnNXVOa0xEV3hmQm1OVm5iWlR2cjN5UFNSR3pQVHpkU3RSRHdQZjV6WVdLYVBQUDJDbXlNT1lZV28ycGRJMXV5c0pvTldOOUJkdUhhTi9VVzVMT0x6V3QzSVFzdEgwODVEUE9hams2cEpOT3g5akN0em1KSUwxd093T2FtU3diNVBHK3pwcllXZ2hKRUpSN0c0Q1VpeG9BMXNkNDJqYlpwbGNDVnNxMWc3c1hrQWdkR3ZGaGNaUDE2ME1TamVHL1dGVXJOWDhQVnpwWUZOenRFUGF1ZnQ4RVdlMmV5dHJNUlo3NTFlTWpPVHowclI0RTVqczl2SFlkajh6dkxNOVU2OHhOOHFYdUtiK25CYm9MbHh4VHQyTjBxNG5YZlliSnQ1QnlmOHloSUdIY1B3cnc0NmJhclM2cDRWRUtsZGFLNWN6ZVpUd0hNNXRQcWhWeHE0ZFNKTjM0R1pYc3ZadFBya3lYeHNTdi9XVXdMZTMydmhLTVk1b0tLdmFmbHJuMVBVamV1aGdOUTRJRitCSngxTjVFSFlQcXVGbFhBamtjVXRFQVpMbkhFVGtwb1VCYVRNaUFGWWdNQVo5NHpuVUJjZ2pxWDg5OGNaa2NYZ25kQUp5b2JINXFrOFk1azdBV3BKeXRDRzlHYXduaTNTVjBnSG1ZMVc4Y0tWaGFxRDB1VFJiRzh6a0k4OXJrZ3Jac0pySjlaWTNWNUhyTGIzWW8vQ3B1MmlOYlBLcUFuTlo0U21KRmlWNyt1bzZINXVmakNmSTVMMXBYVTFLUXRQRXljOWdtR2RKck5peHowMEIvK1p6WGtncHdEZ0c3OTVsWVY1bGFSNzI1UytrbkZCbmc4N3NCR3lDZVFuR0U3RXlLVnNXWkJySFQycnNQRUtjQ0loN2FBd1EyUXRhSmc4SHpadVIya1V3clBWaG9hWmtYZ0FUQjlyWi9abVltcldneEZodEdzVXl0YjdSc0tVVlNLMTROZjF1T25vVDU3NTlaV3dxRFlYOC81TC8zV1FMQmo0SkpxbkVKdlRKN1B0a3FSMFZhT25NcWNyc3VNVVZuek1QUUFWRU4vei81Zk9WeVVhVVd2SjJFOGFUZ3N0dS9VNDM5MkxKeUZNREVzSFgvMUJSb1ZCT3JpMFBHNjdBOGpOZTZReVdwelpXWVd0alFPVHpqS3pGM0dBWEdkM1V5bTV1cEJNeUVCY3d6Mm94UmZYdmpySmxsUHI1cURBQjBRUUdHK1pnSmNiWStTNHhxZ3BBRnlaZ0ZHSStlMTVaak5rUE5sMnpFMzBRbjlBNXVBQTJmcWJBdHl2L2RiMzRwOXRmc1dmWGRVZG8yaytnRytZM3Z3dFJjY0xzQlVnV2VNcXN3ZGpiNlVCc3RHbHY5bnNiZUIzZWlRcExWN1ArYjJCVjhiRzhtZy9LTXBEdGppWVVtQUdlTDB3T1pxSHRJbWRUb2RPQzQvSVdtUFNEd1Q2amhxQkdNTkJidldJYWNhWEhhZGx5L0NNbUxZK0thWTlGRVZ2S0RNWFdvT1ZJajErRldJWUE0MWYvajloU0lHVGhmQkQ5bzN4MWdlRWc0emFLYkVWak5teWF4dzRNMnYvMURhQ3JLZVNwd0RHcXZKNmVqUDRXV0dlamhHamFjelBsZVkycitDQ2grUDhOckZiRm9rYkJ2cHg2YmpqS2o4aWV2em9TZThoRFhnVUxFQmtIRFJubWdWcVdEQVpuR1BsT2U4azdrOE96dDNNWUZBVi9qbFJyemZES2lUUWdtbUZnVkJJMlpSUVlWVi9FNUF6TGFTSEZJTk00Y0FCRUNxVnhxY05xb1pmSnNwZTRqa0MuaVF3a3dRcWNDVDN2SndfTFg1UzlWRERZVE9rTlRYQ3M5U2tsbUNHejRoeWl5bUI1VFNZRllROXhOVHdQSF91bE9DRkhTWmJIRnU0aWhvVFJTVEotc1E%3D&_eventId=submit&submit=SE+CONNECTER' \
  --compressed -o qsdf.txt

curl --cookie-jar ./cookie.txt 'https://ent.uca.fr/' \
  -H 'authority: ent.uca.fr' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="98"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Linux"' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'sec-fetch-site: none' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-user: ?1' \
  -H 'sec-fetch-dest: document' \
  -H 'accept-language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'cookie: device_view=full' \
  --compressed \
  

curl --cookie ./cookie.txt https://ent.uca.fr/scolarite/stylesheets/etu/welcome.faces;jsessionid=022CDA92C8F39644B40AF4C159649058 -s new.html 

