# ZZ2 - Architectures logicielles et qualités

Ressources : <https://perso.isima.fr/~dahill/ALQ/>


## Installation

### Nodejs

L'API Discord requiert que la version de NodeJS soit supérieure à 16.6.
Lien du téléchargement : <https://github.com/nodesource/distributions>

### Discord API

Le plus simple est d'utiliser NPM pour installer le paquet discord.js
``` sudo apt install npm ```

Puis, on installe en local le module discord.js
``` npm install discord.js -save ```
